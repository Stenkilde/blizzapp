(function() {
	'use strict';

	angular.module('blizzardApplication')
		/* @ngInject */
		.config(function ($stateProvider) {

			var Index = {
				name: 'application.main',
				url: '/',
				views: {
					'main@application': {
						templateUrl: 'modules/main/index/index.template.html',
						controller: 'Index',
						controllerAs: 'index'
					}
				}
			};

			 var View = {
			 	name: 'application.resume',
			 	url: '/cv',
			 	views: {
			 		'main@application': {
			 			templateUrl : 'modules/main/resume/resume.template.html',
			 			controller  : 'Resume',
			 			controllerAs: 'resume'
			 		}
			 	}
			};

            var Portfolio = {
                name: 'application.portfolio',
                url: '/portfolio',
                views: {
                    'main@application': {
                        templateUrl : 'modules/main/portfolio/portfolio.template.html',
                        controller  : 'Portfolio',
                        controllerAs: 'portfolio'
                    }
                }
            };

			$stateProvider.state(Index);
			$stateProvider.state(View);
            $stateProvider.state(Portfolio);
		});
})();