var app = angular.module('blizzardApplication', ['ui.router', 'mm.foundation']);

app.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
});